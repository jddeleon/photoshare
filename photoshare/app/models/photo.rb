class Photo < ActiveRecord::Base

has_many :tags, :dependent => :destroy # if a photo is deleted, so are all its tags
belongs_to :user

mount_uploader :image, ImageUploader

validates :title, presence: true
validates :image, presence: true 

def self.title_search title
  if title
    find(:all, :conditions => ['title LIKE ?', "%#{title}%"])
  else
    find(:all)
  end
end

def self.tag_search key, value
  if key && value
    key = key.capitalize
    value = value.capitalize
    photos = Photo.joins(:tags).where(:tags => {:key => key, :value => value})
  end
  return photos
end

end
