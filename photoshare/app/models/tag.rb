class Tag < ActiveRecord::Base
belongs_to :photo # basically a foreign key pointing to a photo object

validates :key, :value, presence: true
before_save {self.key = key.capitalize}
before_save {self.value = value.capitalize}

end
