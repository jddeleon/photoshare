class TagsController < ApplicationController
  #before_action :set_tag, only: [:destroy]
  before_filter :authenticate_user!

  # GET /tags
  # GET /tags.json
  def index
    @tags = Tag.all
  end

  # GET /tags/1
  # GET /tags/1.json
  def show
    @tag = Tag.find(params[:id])
  end

  # GET /tags/new
  def new
    @photo = Photo.find(params[:photo_id])
    @tag = @photo.tags.new
  end

  # GET /tags/1/edit
  def edit
    @tag = Tag.find(params[:id])
    if @tag.photo.user != current_user
      redirect_to @tag, alert: 'You cannot edit someone elses Tags!'
    end
  end

  # POST /tags
  # POST /tags.json
  def create
    @photo = Photo.find(params[:photo_id])
    @tag = Tag.new(tag_params)
    @tag.photo = @photo
    

    respond_to do |format|
      if @tag.save
        format.html { redirect_to @tag.photo, notice: 'Tag was successfully created.' }
        format.json { render action: 'show', status: :created, location: @tag }
      else
        format.html { render action: 'new' }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tags/1
  # PATCH/PUT /tags/1.json
  def update
    @tag = Tag.find(params[:id])

    respond_to do |format|
      if @tag.update(tag_params)
        format.html { redirect_to @tag, notice: 'Tag was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tags/1
  # DELETE /tags/1.json
  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy
    respond_to do |format|
      format.html { redirect_to edit_photo_path(@tag.photo_id), notice: 'Tag successfully deleted' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @photo = Photo.find(params[:id])
      @tag = @photo.tags.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_params
      params.require(:tag).permit(:key, :value, :photo_id)
    end
end
