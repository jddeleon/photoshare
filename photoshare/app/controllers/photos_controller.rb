class PhotosController < ApplicationController
 # before_action :set_photo, only: [#:edit, :update, :destroy]
  before_filter :authenticate_user!
  
  # Search photos by title
  def title_search
    @results = Photo.title_search(params[:search])

    if !@results.empty?
      flash[:notice] = "Showing Photos with title \"#{params[:search]}\""
    else
      flash[:alert] = "No Photos were found matching the title \"#{params[:search]}\"" 
    end
    
    return @results
  end

  # search photos by tag
  def tag_search
    @results = Photo.tag_search(params[:key], params[:value])

    if !@results.empty?
      flash[:notice] = "Showing Photos Tagged with Key = \"#{params[:key]}\" and Value =  \"#{params[:value]}\""
    else
      flash[:alert] = "No Photos Found Tagged With Key = \"#{params[:key]}\" and Value =  \"#{params[:value]}\""
    end
    
    return @results
  end
  
  def tag_search_form
  end
  
  # GET /photos
  # GET /photos.json
  def index
    @photos = Photo.all
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @photo = Photo.find(params[:id])
  end

  # GET /photos/new
  def new
    @user = User.find(current_user.id)
    @photo = @user.photos.new #new photo in the context of @user
  end

  # GET /photos/1/edit
  def edit
    @photo = Photo.find(params[:id])
    if @photo.user != current_user
      redirect_to @photo, alert: 'You cannot edit someone elses photos!'
    end
  end

  # POST /photos
  # POST /photos.json
  def create
    @user = User.find(params[:user_id])
    @photo = Photo.new(photo_params)
    @photo.user = @user

    respond_to do |format|
      if @photo.save
        format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
        format.json { render action: 'show', status: :created, location: @photo }
      else
        format.html { render action: 'new' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    @photo = Photo.find(params[:id])
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photos_url, notice: 'Photo was sucessfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @user = User.find(params[:user_id])
      @photo = @user.photos.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:title, :image, :user_id)
    end
end
