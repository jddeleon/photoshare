json.array!(@tags) do |tag|
  json.extract! tag, :key, :value, :photo_id
  json.url tag_url(tag, format: :json)
end
